The Contentin module dynamically registers migrations from CSV files.
Field/subfield columns and muti-value fields in the CSV file are supported for
many Drupal field widgets. The current default field separators are three
underscores for field and subfield, and two underscores for multi-value fields.

Currently, it is recommended to use the Contentout module to create the CSV
files. Both modules appear in the Content Automation portion of the modules
page.

The default directory for uploading images is under the contentin module:
. . . /contentin/uploads

This module depends on the Migrate module (http://drupal.org/project/migrate)
to dynamically register migrations from CSV files.

Compatibility
-------------
The Contentin requires Migrate V2.6 or later.


Maintainer
-----------
Hector Iribarne https://www.drupal.org/user/192646
